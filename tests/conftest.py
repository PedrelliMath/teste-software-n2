from stat_funcs import StatsN2
import pytest


@pytest.fixture(scope='session')
def class_generator():
    print('Entregando classe StatsN2...')
    yield StatsN2
    print('fixture encerrada...')