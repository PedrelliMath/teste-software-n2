import pytest

@pytest.mark.variancia
@pytest.mark.parametrize("test_input", [([10, 20, 30]), ([30, 40, 50])])
def test_variancia_may_return_100(class_generator, test_input):
    assert class_generator.variancia(test_input) == 100.0

@pytest.mark.variancia
def test_variancia_may_return_1(class_generator):
    assert class_generator.variancia([10,11,12]) == 1.0

@pytest.mark.variancia
@pytest.mark.xfail
def test_variancia_may_return_xfail(class_generator):
    assert class_generator.variancia() == 100.0