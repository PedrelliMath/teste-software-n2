import pytest

@pytest.mark.media
@pytest.mark.parametrize(
        "test_input,expected",
        [([10,10], 10), ([5, 5], 5)]
    )
def test_media_may_return_10_5(class_generator, test_input, expected):
    assert class_generator.media(test_input) == expected

@pytest.mark.media
def test_media_may_return_0(class_generator):
    assert class_generator.media([]) == 0

@pytest.mark.media
@pytest.mark.xfail
def test_media_may_return_xfail(class_generator):
    assert class_generator.media([{"valor":50}]) == 50
    