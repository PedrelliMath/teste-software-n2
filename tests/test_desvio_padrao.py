import pytest

@pytest.mark.desvio_padrao
@pytest.mark.parametrize("test_input", [([10, 20, 30]), (30, 40, 50)])
def test_desvio_padrao_may_return_10(class_generator, test_input):
    variancia = class_generator.variancia(test_input)
    assert class_generator.dpadrao(variancia) == 10.0

@pytest.mark.desvio_padrao
def test_desvio_padrao_may_return_5(class_generator):
    variancia = class_generator.variancia([10,15,20])
    assert class_generator.dpadrao(variancia) == 5.0

@pytest.mark.desvio_padrao
@pytest.mark.xfail
def test_desvio_padrao_may_xfail(class_generator):
    assert class_generator.dpadrao(0) == 50