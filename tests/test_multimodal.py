import pytest

@pytest.mark.multimodal
@pytest.mark.parametrize(
    "test_input,expected", 
    [
        ([10, 10, 20, 20, 30], [10, 20]),
        ([50, 50, 20, 20, 10], [50, 20])
    ]
)
def test_multimodal_may_return_10_20_50_20(class_generator, test_input, expected):
    assert class_generator.multimodal(test_input) == expected

@pytest.mark.multimodal
def test_multimodal_may_return_not_multimodal(class_generator):
    assert "Não é multimodal" in class_generator.multimodal(([10, 10, 20]))

@pytest.mark.multimodal
@pytest.mark.xfail
def test_multimodal_may_return_xfailure(class_generator):
    assert "Não é multimodal" in class_generator.multimodal()