import pytest

@pytest.mark.amodal
@pytest.mark.parametrize("test_input", [([1,2,3]), ([4,5,6]), ([7,8,9])])
def test_amodal_may_return_not_exists(class_generator, test_input):
    assert "Não existe moda" in class_generator.amodal(test_input)

@pytest.mark.amodal
def test_amodal_may_return_exists(class_generator):
    assert "Existe moda" in class_generator.amodal([1,1,3,4])
    
@pytest.mark.amodal
@pytest.mark.xfail
def test_amodal_may_fail(class_generator):
    assert class_generator.amodal() == 10