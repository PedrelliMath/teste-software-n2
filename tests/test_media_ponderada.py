import pytest

@pytest.mark.media_ponderada
@pytest.mark.parametrize("a,b,expected", [([10, 8], [1, 1], 9), ([8, 7], [1, 1], 7.5)])
def test_media_p_may_return(class_generator, a, b, expected):
    assert class_generator.media_ponderada(a, b) == expected

@pytest.mark.media_ponderada
def test_media_p_may_return_9(class_generator):
    assert class_generator.media_ponderada([10, 8], [1, 1]) == 9

@pytest.mark.media_ponderada
@pytest.mark.xfail
def test_media_p_may_failure(class_generator):
    assert class_generator.media_ponderada([10, 8], [1, 1]) == "N2"