import pytest

@pytest.mark.skew
@pytest.mark.parametrize("test_input", [([10, 20, 30]), (30, 40, 50)])
def test_skew_may_return_normal(class_generator, test_input):
    assert "Distribuição normal" == class_generator.skew(test_input)

@pytest.mark.skew
def test_skew_may_return_positive(class_generator):
    assert "Distribuição positiva" == class_generator.skew([10,100,500])

@pytest.mark.skew
def test_skew_may_return_negative(class_generator):
    assert "Distribuição negativa" == class_generator.skew([-100, 20, 10])

@pytest.mark.skew
@pytest.mark.xfail
def test_skew_may_return_xfail(class_generator):
    assert "Distribuição negativa" == class_generator.skew()

