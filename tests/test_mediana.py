import pytest

@pytest.mark.mediana
@pytest.mark.parametrize("test_input,expected", [([1,1,4,5], 2.5), ([2,3,4,5], 3.5)])
def test_mediana_may_return_2_5_3_5(class_generator, test_input, expected):
    assert class_generator.mediana(test_input) == expected

@pytest.mark.mediana
def test_mediana_may_return_1(class_generator):
    assert class_generator.mediana([1,1,2,2]) == 1.5

@pytest.mark.mediana
def test_mediana_may_return_2(class_generator):
    assert class_generator.mediana([1,1,2,2,2]) == 2

@pytest.mark.mediana
def test_mediana_may_return_0(class_generator):
    assert class_generator.mediana([]) == 0

@pytest.mark.mediana
@pytest.mark.xfail
def test_mediana_may_return_xfail(class_generator):
    assert class_generator.mediana((1,2,3,4)) == 9