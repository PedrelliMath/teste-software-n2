import pytest

@pytest.mark.unimodal
@pytest.mark.parametrize("test_input", [([1]), ([4]), ([6])])
def test_unimodal_may_return_first_item(class_generator, test_input):
    assert class_generator.unimodal(test_input) == test_input[0]

@pytest.mark.unimodal
def test_unimodal_may_return_not_unimodal(class_generator):
    assert "Não é unimodal" == class_generator.unimodal([1,2,3,4,5,6,7,8])

@pytest.mark.unimodal
@pytest.mark.xfail
def test_unimodal_may_return_xfail(class_generator):
    assert "Não é unimodal" in class_generator.unimodal()
